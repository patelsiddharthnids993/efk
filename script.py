import subprocess
import time

def countdown(t):
        #t = 10
        while t:
                mins, secs = divmod(t, 60)
                timer = '{:02d}:{:02d}'.format(mins, secs)
                print(timer, end="\r")
                time.sleep(1)
                t -= 1

        print('Times up !!!')

def creating():

    print("*******************************************************")
    print("Creating Persistent Volume for elasticsearch statefulset")
    subprocess.call("kubectl create -f pv.yml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Creating  the Namespace EFK")
    subprocess.call("kubectl create -f ns.yml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Creating Elasticsearch Statefulset and Persistent Volume Claim")
    subprocess.call("kubectl create -f elasticsearch/es-sts.yaml", shell=True)
    print("Sleeping for 50 seconds !!!")
    countdown(50)
    print("*******************************************************")

    print("Creating Service for Elasticsearch Statefulset")
    subprocess.call("kubectl create -f elasticsearch/es-svc.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Creating Kibana Deployment")
    subprocess.call("kubectl create -f kibana/kibana-deployment.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Creating Kibana Service")
    subprocess.call("kubectl create -f kibana/kibana-svc.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Creating Service Account for Fluentd")
    subprocess.call("kubectl create -f fluentd/fluentd-sa.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Creating Role for Service Account")
    subprocess.call("kubectl create -f fluentd/fluentd-role.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Creating RoleBinding for Service Account and Role")
    subprocess.call("kubectl create -f fluentd/fluentd-rb.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Creating ConfigMap for fluentd")
    subprocess.call("kubectl create cm myconfig --from-file=fluentd/kubernetes.conf -n efk", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Creating Fluentd DaemonSet")
    subprocess.call("kubectl create -f fluentd/fluentd-ds.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*****************DONE**************************")

def deleting():

    print("*******************************************************")
    print("Deleting Fluentd DaemonSet")
    subprocess.call("kubectl delete -f fluentd/fluentd-ds.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Deleting ConfigMap for fluentd")
    subprocess.call("kubectl delete cm myconfig -n efk", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Deleting RoleBinding for Service Account and Role")
    subprocess.call("kubectl delete -f fluentd/fluentd-rb.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Deleting Role for Service Account")
    subprocess.call("kubectl delete -f fluentd/fluentd-role.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Deleting Service Account for Fluentd")
    subprocess.call("kubectl delete -f fluentd/fluentd-sa.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Deleting Kibana Service")
    subprocess.call("kubectl delete -f kibana/kibana-svc.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Deleting Kibana Deployment")
    subprocess.call("kubectl delete -f kibana/kibana-deployment.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Deleting Service for Elasticsearch Statefulset")
    subprocess.call("kubectl delete -f elasticsearch/es-svc.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Deleting Elasticsearch Statefulset and Persistent Volume Claim")
    subprocess.call("kubectl delete -f elasticsearch/es-sts.yaml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Deleting Elasticsearch Statefulset and Persistent Volume Claim")
    subprocess.call("kubectl delete pvc --all -n efk", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Deleting Persistent Volume for elasticsearch statefulset")
    subprocess.call("kubectl delete -f pv.yml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("*******************************************************")

    print("Deleting  the Namespace EFK")
    subprocess.call("kubectl delete -f ns.yml", shell=True)
    print("Sleeping for 5 seconds !!!")
    countdown(5)
    print("******************DONE******************************")


def main_function():
    print('''Enter 1 to create resources
Enter 2 to delete resources''')
    int1 = int(input())
    if (int1 == 1):
        creating()
    elif (int1 == 2):
        deleting()

main_function()
